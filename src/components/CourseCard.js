// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {useState, useEffect, useContext} from "react";
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';

export default function CourseCard({courseProp}){
	/*console.log(props)*/
	const { _id, name, description, price, slots} = courseProp

	// use the state hook for this component to be able to store its state specifically to monitor the slots available

	// states are used to keep track information related to individual components

	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots) 
	
	const [isAvailable, setIsAvailable] = useState(true)

	const {user} = useContext(UserContext);

	// Add an "useEffect" hook to have "CourseCard" component do perform a certain task afte every DOM update.

	// Syntax: useEffect(functuinToBeTriggered,statesToBeMonitored])

	useEffect(() => {
		if(slotsAvailable === 0){
			setIsAvailable(false);
		}
 		
	}, [slotsAvailable])

	// setEnrollees(1)

	// function enroll(){
	// 	if(slotsAvailable === 1){ 
	// 		alert("That's the last available Slots!")
	// 	}
	// 	setEnrollees(enrollees + 1)
	// 	setSlotsAvailable(slotsAvailable - 1)
	// }
	
	return(
		<Row className = "my-3">
		{/* First Card*/}
			<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
				<Card>
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        {/*Card.Subtitle*/}
			        <Card.Subtitle className = "h6">Description</Card.Subtitle>
			        <Card.Text className = "small">{description}</Card.Text>

			        <Card.Subtitle className = "h6">Price:</Card.Subtitle>
			        <Card.Text>PHP {price}</Card.Text>

			        <Card.Subtitle className = "h6">Enrollees:</Card.Subtitle>
			        <Card.Text>{enrollees}</Card.Text>

			        <Card.Subtitle className = "h6">Slots Available:</Card.Subtitle>
			        <Card.Text>{slotsAvailable} slots</Card.Text>
			        {
			        	(user !== null) ?
			        	<Button as = {Link} to = {`/courses/${_id}`} variant="primary" disabled = {!isAvailable}>Details</Button>
			        	:
			        	<Button as = {Link} to = '/login'variant="primary" disabled = {!isAvailable}>Enroll</Button>
			        }

			      </Card.Body>
			    </Card>
			</Col>
		</Row>
		)
}