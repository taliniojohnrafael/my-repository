// /* Bootstrap Grid System */
// importing using deconstruction
import {Button, Row, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useContext, useEffect, useState} from 'react';
import Cards from '../components/Cards';


export default function Dashboard(){
    const [productData, setProductData] = useState([]);


    useEffect(() => {
        fetch(`http://localhost:4000/products/allProducts`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization : `Bearer ${localStorage.getItem(`token`)}`
            }
        }).then(response => response.json())
        .then(data =>{
            setProductData(data.map(product => {
                return (<Cards key={product._id} product={product}/>)
            }))
        })
    },[])



	return(
        <Container className='mt-4'>
            <h2> Coffee ~ Products </h2>
            <div className='mb-2 mt-4 text-start'>
                <Button className= "btn btn-primary mx-1" as = {Link} to ="/createproduct">Create</Button>
                <a className= "btn btn-danger mx-1">Remove</a>
            </div>
            <div className='text-center'>
                {productData}
            </div>
                
        </Container>
		)
}